﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builder
{
    public class PartnerBuilder
    {
        private Partner _partner { get; set; }
        public PartnerBuilder()
        {
            _partner = new Partner()
            {
                Id =new Guid(),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()                
            };
        }
        public PartnerBuilder SetId(Guid id)
        {
            _partner.Id = id;
            return this;
        }
        public PartnerBuilder SetIsActive(bool status)
        {
            _partner.IsActive = status;
            return this;
        }

        public PartnerBuilder SetNumberPromocodes(int number)
        {
            _partner.NumberIssuedPromoCodes = number;
            return this;
        }

        public PartnerBuilder AddSamplePromocodeLimit()
        {
            _partner.PartnerLimits.Add(
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = new DateTime(2024, 1, 1),
                    EndDate = new DateTime(2024, 1, 10),
                    Limit = 10
                }
            );
            return this;
        }
        public PartnerBuilder AddSamplePromocodeLimitWithCancelDate(DateTime date)
        {
            _partner.PartnerLimits.Add(
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = new DateTime(2024, 1, 1),
                    CancelDate = date,
                    Limit = 10
                }
            );
            return this;
        }

        public PartnerBuilder NulleblePromocodeLimit()
        {
            _partner.PartnerLimits = null;
            return this;
        }

        public Partner GetCurrentPartner()
        {
            return _partner;
        }
    }
}

