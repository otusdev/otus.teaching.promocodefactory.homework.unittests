﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builder;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {      
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;      
        private readonly PartnersController _partnersController;       

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();         
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = new Guid ("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            var setPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partner = new PartnerBuilder()
                .AddSamplePromocodeLimit()
                .SetIsActive(false)
                .GetCurrentPartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, new SetPartnerPromoCodeLimitRequest());

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        ///Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNumberIssuedPromoCodes_EqualZero()
        {
            // Arrange
            var partner = new PartnerBuilder()                
                .AddSamplePromocodeLimit()
                .SetNumberPromocodes(1000)
                .GetCurrentPartner();

            Guid partnerId = new Guid ("def47943-7aaf-44a1-ae21-05aa4948b165");
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);           

            // Assert         
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// Если партнеру выставляется лимит, который закончился, то количество NumberIssuedPromoCodes не обнуляется
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_RequestLimitMoreThenAndExistingLimitIsCanceled_NumberIssuedPromoCodesIsNotReset()
        {
            var limit = 10;
            Guid partnerId = new Guid("def47943-7aaf-44a1-ae21-05aa4948b165");
            // Arrange      
            var partner = new PartnerBuilder()
                .SetId(partnerId)
                .AddSamplePromocodeLimitWithCancelDate(DateTime.Now)
                .SetNumberPromocodes(limit)
                .GetCurrentPartner();

            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();
            _partnersRepositoryMock
                .Setup(r => r.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);        
          
            // Assert 
            partner.NumberIssuedPromoCodes.Should().Be(limit);
        }

        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_RequestLimitMoreThen0AndExistingLimitNotCanceled_NumberIssuedPromoCodesIsNotReset()
        {
            // Arrange          
            var partner = new PartnerBuilder()
               .AddSamplePromocodeLimit()
               .GetCurrentPartner();

            Guid partnerId = new Guid("def47943-7aaf-44a1-ae21-05aa4948b165");

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            DateTime excpectedAfterDateTime = DateTime.Now;
            var exceptedExistingLimit = partner.PartnerLimits.OrderByDescending(x=>x.CancelDate).First();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            exceptedExistingLimit.CancelDate.Should()
                .BeAfter(excpectedAfterDateTime)
                .And
                .BeBefore(DateTime.Now);
        }

        /// <summary>
        ///Лимит должен быть больше 0
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_RequestLimitLess0_ReturnsBadRequest()
        {
            // Arrange
            var partner = new PartnerBuilder()               
               .GetCurrentPartner();
            Guid partnerId = new Guid("def47943-7aaf-44a1-ae21-05aa4948b165");
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            var request = new Fixture()
                .Build<SetPartnerPromoCodeLimitRequest>()
                .With(r => r.Limit, -100)
                .Create();
            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>("Лимит должен быть больше 0");
        }

        /// <summary>      
        ///Нужно убедиться, что сохранили новый лимит в базу данных;
        /// </summary>        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTest_ReturnsCreatedAtAction()
        {
            var partner = new PartnerBuilder().AddSamplePromocodeLimit().GetCurrentPartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 100,
                EndDate = DateTime.Now.AddMonths(3)
            };          

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }
    }
}