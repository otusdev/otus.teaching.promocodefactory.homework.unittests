﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class Response<T>
    {
        public T? Result { get; set; }
        public string Error { get; set; }
        public string TypeError { get; set; }
        public Guid? Id { get; set; }
    }
}
