﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Helpers
{
    public static class CommonHelper
    {
        public static  string FormatDate(DateTime? date)
        {
            return date?.ToString("dd.MM.yyyy hh:mm:ss");
        }
    }
}
