﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Helpers
{
    public class LimitHelper
    {
        public static Response<PartnerPromoCodeLimit> SetPromoCodeLimit(Partner partner, SetPartnerPromoCodeLimitRequest request)
        {
            var result = new Response<PartnerPromoCodeLimit>();
            if (!partner.IsActive)
            {
                result.Error = Message.PartnerIsNotActive;
            }

            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue);

            if (activeLimit != null)
            {
                partner.NumberIssuedPromoCodes = 0;
                activeLimit.CancelDate = DateTime.Now;
            }

            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = request.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = request.EndDate
            };

            partner.PartnerLimits.Add(newLimit);
            result.Id = newLimit.Id;
            return result;
        }

    }
}
